import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  public name: string;
  public myId: number;
  public isDisabled: boolean;
  public successClass: string;
  public hasError: boolean;
  public isSpecial: boolean;
  public messageClasses: object;
  public highlightColor: string;
  public titleStyles: object;
  public greeting: string;

  constructor() {
    this.name = 'Gredler';
    this.myId = 10;
    this.isDisabled = true;
    this.successClass = 'text-success';
    this.hasError = true;
    this.isSpecial = true;
    this.messageClasses = {
      'text-success': !this.hasError,
      'text-danger' : this.hasError,
      'text-special' : this.isSpecial
    };
    this.highlightColor = 'orange';
    this.titleStyles = {
      color: 'blue',
      fontStyle: 'italics'
    };
    this.greeting = '';
  }

  ngOnInit() {
  }

  greetUser() {
    return 'Hello ' + this.name + '!';
  }

  onClick(event) {
    this.greeting = 'Hi';
    console.log(event);
  }

  logMessage(value) {
    console.log(value);
  }
}
