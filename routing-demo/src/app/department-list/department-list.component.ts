import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  public departments: object[];
  public selectedId: number;

  constructor(private router: Router, private route: ActivatedRoute) {
    this.departments = [
      {'id': 1, 'name': 'Angular'},
      {'id': 2, 'name': 'Bootstrap'},
      {'id': 3, 'name': 'Spring'},
      {'id': 4, 'name': 'Ruby'},
    ];
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.selectedId = parseInt(params.get('id'), 10))
    );
  }

  onSelect(department) {
    // this.router.navigate(['/departments', department.id]);
    this.router.navigate([department.id], {relativeTo: this.route});
  }

  isSelected(department) {
    return department.id === this.selectedId;
  }
}
