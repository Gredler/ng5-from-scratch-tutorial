import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.css']
})
export class DepartmentDetailComponent implements OnInit {
  public departmentId: number;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    // this.departmentId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.departmentId = parseInt(params.get('id'), 10))
    );
  }

  goToPrevious() {
    const previousId = this.departmentId - 1;
    // this.router.navigate(['/departments/', previousId]);
    this.router.navigate(['../', previousId], {relativeTo: this.route});
  }

  goToNext() {
    const nextId = this.departmentId + 1;
    // this.router.navigate(['/departments/', nextId]);
    this.router.navigate(['../', nextId], {relativeTo: this.route});
  }

  goToDepartments() {
    const selectedId = this.departmentId ? this.departmentId : null;
    // this.router.navigate(['/departments', { id: selectedId }]);
    this.router.navigate(['../', { id: selectedId }], { relativeTo: this.route });
  }

  showOverview() {
    this.router.navigate(['overview'], {relativeTo: this.route});
  }

  showContact() {
    this.router.navigate(['contact'], {relativeTo: this.route});
  }
}
