import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  public displayName: boolean;
  public color: string;
  public colors: string[];
  @Input() public parentData: string;
  @Output() public childEvent = new EventEmitter();

  public message: string;
  public name: string;
  public names: object;

  public date: Date;

  constructor() {
    this.displayName = true;
    this.color = 'blue';
    this.colors = ['red', 'blue', 'green', 'yellow'];
    this.message = 'Hey look at me';
    this.names = {
      firstName: 'John',
      lastName: 'Doe'
    };
    this.date = new Date();
  }

  ngOnInit() {
  }

  fireEvent() {
    this.childEvent.emit(this.message);
  }

}
