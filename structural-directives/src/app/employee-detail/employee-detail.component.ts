import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { IEmployee } from '../employee';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  public employees: IEmployee[];
  public errorMessage: String;

  constructor(private _employeeService: EmployeeService) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this._employeeService.getEmployees()
      .subscribe(
        data => this.employees = data,
        error => this.errorMessage = error
      );
  }

}
